
export interface productos{
    precio: number;
    candidad: number;
    codigo: number;
    nombre: string;
    peso: number;
    descripcion: string;
    proveedor: proveedor[];
}
export interface proveedor{
    nombre: string;
    cedula: number;
    direccion: string;
    numero: number;
    correo:string;
    codigo: number;
}
