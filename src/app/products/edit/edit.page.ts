import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ProductService } from "../product.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { productos } from '../products.model';


@Component({
  selector: "app-edit",
  templateUrl: "./edit.page.html",
  styleUrls: ["./edit.page.scss"],
})
export class EditPage implements OnInit {
  product: productos;
  formProductEdit: FormGroup;

  constructor(
    private activeRouter: ActivatedRoute,
    private serviceProduct: ProductService,
    private router: Router
  ) { }

  ngOnInit() {
    this.activeRouter.paramMap.subscribe(paramMap => {
      if (!paramMap.has('productId')) {
        return;
      }
      const productId = parseInt(paramMap.get('productId'));
      this.product = this.serviceProduct.getProduct(productId);
    });

    this.formProductEdit = new FormGroup({
      pcantidad: new FormControl(this.product.candidad,
        {
          updateOn: "blur",
          validators: [Validators.required, Validators.min(1)],
        }
      ),
      pcodigo: new FormControl(this.product.codigo,
        {
          updateOn: "blur",
          validators: [Validators.required, Validators.minLength(3)],
        }
      ),
      pnombre: new FormControl(this.product.nombre,
        {
          updateOn: "blur",
          validators: [Validators.required, Validators.minLength(3)],
        }
      ),
      ppeso: new FormControl(this.product.peso,
        {
          updateOn: "blur",
          validators: [Validators.required, Validators.min(1)],
        }
      ),
      pdescripcion: new FormControl(this.product.descripcion,
        {
          updateOn: "blur",
          validators: [Validators.required, Validators.minLength(30)],
        }
      ),
    });

    this.formProductEdit.value.pnombre = this.product.nombre;

  }

  editProduct() {
    if (!this.formProductEdit.valid) {
      return;
    }
    this.serviceProduct.editProduct(
      this.formProductEdit.value.pprecio,
      this.formProductEdit.value.pcantidad,
      this.formProductEdit.value.pcodigo,
      this.formProductEdit.value.pnombre,
      this.formProductEdit.value.ppeso,
      this.formProductEdit.value.pdescripcion
    );
    this.formProductEdit.reset();
    this.router.navigate(['./products']);
  }
}