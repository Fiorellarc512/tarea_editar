import { Injectable } from '@angular/core';
import { productos, proveedor } from './products.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private proveedore: proveedor[] = [
    {
      nombre: "Artisian",
      cedula: 11111,
      direccion: "San Jose",
      numero: 88889090,
      correo: "artisian.p@gmail.com",
      codigo: 492
    },
  ];
  private productos: productos[] = [
    {
      proveedor: this.proveedore,
      precio: 7000,
      candidad: 5,
      codigo: 1283,
      nombre: "Piercing Industrial",
      peso: 11,
      descripcion: "Tipo de piercing: Industrial, material: plata 925"
    },
    {
      proveedor: this.proveedore,
      precio: 5000,
      candidad: 12,
      codigo: 4802,
      nombre: "Piercing nariz",
      peso: 8,
      descripcion: "Tipo de piercing: Nariz, material: plata 925"
    },
    {
      proveedor: this.proveedore,
      precio: 8500,
      candidad: 4,
      codigo: 9021,
      nombre: "Piercing Cartílago",
      peso: 6,
      descripcion: "Tipo de piercing: Cartílago (concha, hélix...), material: oro"
    }
  ];
  constructor() { }
  getAll() {
    return [...this.productos];
  }
  getProduct(productId: number) {
    return {
      ...this.productos.find(
        product => {
          return product.codigo === productId;
        }
      )
    };
  }
  deleteProduct(productId: number) {
    this.productos = this.productos.filter(
      product => {
        return product.codigo !== productId;
      }
    );
  }
  addProduct(
    pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pdescripcion: string
  ) {
    const product: productos = {
      proveedor: this.proveedore,
      precio: pprecio,
      candidad: pcantidad,
      codigo: pcodigo,
      nombre: pnombre,
      peso: ppeso,
      descripcion: pdescripcion
    }
    this.productos.push(product);
  }
  editProduct(
    pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pdescripcion: string
  ) {
    let index = this.productos.map((x) => x.codigo).indexOf(pcodigo);

    this.productos[index].codigo = pcodigo;
    this.productos[index].candidad = pcantidad;
    this.productos[index].nombre = pnombre;
    this.productos[index].peso = ppeso;
    this.productos[index].descripcion = pdescripcion;
    this.productos[index].precio = pprecio;

    console.log(this.productos);
  }
}
