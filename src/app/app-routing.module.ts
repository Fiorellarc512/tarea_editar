import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'products',
    children:[
      {
        path: '',
        loadChildren: './products/products.module#ProductsPageModule'
      },
      {
        path: 'add',
        loadChildren: './products/add/add.module#AddPageModule' 
      },
      {
        path: ':productId',
        loadChildren: './products/detail/detail.module#DetailPageModule'
      },
      {
        path: "edit/:productId",
        loadChildren: "./products/edit/edit.module#EditPageModule",
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
